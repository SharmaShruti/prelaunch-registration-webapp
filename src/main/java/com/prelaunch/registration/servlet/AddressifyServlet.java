package com.prelaunch.registration.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import lombok.extern.slf4j.Slf4j;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/getAddress")
@Slf4j
public class AddressifyServlet extends HttpServlet {

	private static final String AUTOCOMPLETE_API_URL = "https://api.addressify.com.au/address/autocomplete";
	private static final String QUERY_PARAM_API = "?api_key=";
	private static final String API_KEY = "ea3060f1-36e6-484e-836e-77f96e3802fa";
	private static final String QUERY_PARAM_TERM = "&term=";
	private static final long serialVersionUID = 1L;

	@Override
	public void init() {
		log.info("Loading AddressifyServlet...");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		log.info("Entering User Login Servlet : Post call");
		request.getParameterMap().entrySet().stream()
				.forEach(entry -> {
					log.info(entry.getKey() + " = " + String.join(",", entry.getValue()));
				});
 		doGet(request, response);
 	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		log.info("Entering User Login Servlet : Get call from Post");
		
		String searchList = new Gson().toJson(getAddressList(request));
		log.info("Sending valid address list to client : " + searchList);
		response.addHeader("Content-type", "application/json");
		response.getWriter().write(searchList);
	}

	/**
	 * 
	 * @param request
	 * @return AddressList from the AddresLite Autocomplete API based on user entered address string 
	 * 
	 * Returns an array of complete address strings for the given incomplete address string. 
	 * Unit/Level/Street number information is piped through from the user input. 
	 */
	private List<String> getAddressList(HttpServletRequest request) {
		
		log.info("Fetching valid address list from Addressfiy API");

		HttpURLConnection conn = null;
		List<String> addressList = new ArrayList<>();
		  
		try {
			
			URL url = new URL(getAutocompleteAPIURL(request.getParameter("userAddrInput")));

			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			
			int responseCode = conn.getResponseCode();
			log.info("Response Code : " + responseCode);

			if(responseCode == HttpURLConnection.HTTP_OK) {
				BufferedReader br = new BufferedReader(new InputStreamReader(
						(conn.getInputStream())));

				ObjectMapper mapper = new ObjectMapper();
				addressList = Arrays.asList(mapper.readValue(br.readLine(), String[].class));
				log.info("Output from Server .... \n");
				addressList.stream().forEach(address -> log.info(address));
			}
			else if(responseCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
				log.error("Address validation request is Unathorized and failed with response message : " + conn.getResponseMessage() + " & response code : " + responseCode);
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}
			else if(responseCode == HttpURLConnection.HTTP_BAD_REQUEST) {
				log.error("Address validation request is Bad Request and failed with response message : " + conn.getResponseMessage() + " & response code : " + responseCode);
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}

		} catch (MalformedURLException e) {

			log.error("Something went wrong in login servlet, MalformedURLException caught!", e);

		}
		catch (IOException e) {
			log.error("Something went wrong in login servlet, IOException caught!", e);

		} finally {
			conn.disconnect();
		}
		return addressList;
	}
	
	/**
	 * 
	 * @param term
	 * @return URL for invoking the addresslite autocomplete api like - https://api.addressify.com.au/address/autocomplete?api_key={your API key}&term=1+george+st+t
	 */
	private String getAutocompleteAPIURL(String term) {

		StringBuilder url = new StringBuilder();
		url.append(AUTOCOMPLETE_API_URL);
		url.append(QUERY_PARAM_API);
		url.append(API_KEY);
		url.append(QUERY_PARAM_TERM);
 
		try {
			String param = URLEncoder.encode(term, StandardCharsets.UTF_8.toString());
			log.info("Encoded param : " + param);
			url.append(param);
		}
		catch(UnsupportedEncodingException e) {
			log.error("Something went wrong in building addresify autocomplete api URL, UnsupportedEncodingException caught!", e);
		}

		log.info("URL for Addressify Autocomplete API : " + url);
		return url.toString();
	}
}