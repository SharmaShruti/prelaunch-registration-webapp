package com.prelaunch.registration.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.prelaunch.registration.dao.UserDao;
import com.prelaunch.registration.model.User;

/**
 * Servlet implementation class UserRegistrationServlet
 */
@WebServlet("/registerUser")
@Slf4j
public class UserRegistrationServlet extends HttpServlet {
	
	private static final String PARSE_API_URL = "https://api.addressify.com.au/address/parse";
	private static final String VALIDATE_API_URL = "https://api.addressify.com.au/address/validate";
	private static final String QUERY_PARAM_API = "?api_key=";
	private static final String API_KEY = "ea3060f1-36e6-484e-836e-77f96e3802fa";
	private static final String QUERY_PARAM_TERM = "&term=";
	private static final long serialVersionUID = 1L;
	private UserDao userDao;
	private String streetAddress;
	private String suburb;
	private String state;
	private String postcode;
	
	@Override
	public void init() {
		log.info("Initializing pre-launch user registration web app...");
		userDao = new UserDao();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		log.info("Entering User Regsitration Servlet : Get call from Post");

		try {
			registerUser(request, response);
		} catch (Exception e) {
			log.error("Something went wrong in User Regsitration Servlet, Exception caught!", e);
		}
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		log.info("Entering User Regsitration Servlet : Post call");
		doGet(request, response);
	}
	/**
	 * 
	 * @param request
	 * @param response
	 * @throws Exception
	 * 
	 * Registers the User with given detail and saves the user details into database
	 */
	private void registerUser(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		log.info("Registering User Details");
		
		String firstName = request.getParameter("firstName");
		String middleName = request.getParameter("middleName");
		String lastName = request.getParameter("lastName");		
		String dob = request.getParameter("dob");
		String gender = request.getParameter("gender");
		String contact = request.getParameter("contact");
		String email = request.getParameter("email");
		String address = request.getParameter("address");
		
		try {
			
			log.info("Validate if user entered address in valid or not. If valid, proceed for saving details onto database else return back error.");
			if(validateAddress(address)) {
				
				log.info("Proceed for saving details onto database.");
				User user = new User();
				user.setFirstName(firstName);
				user.setMiddlename(middleName);
				user.setLastName(lastName);
				user.setDob(dob);
				user.setGender(gender);
				user.setContact(contact);
				user.setEmail(email);
				
				// Parsing the address through addresslite parse api to save into the database correctly
				parseAddress(address);
				user.setStreetAddress(streetAddress);
				user.setSuburb(suburb);
				user.setState(state);
				user.setPostcode(postcode);
				
				if (userDao.saveUserDetails(user)) {
					log.info("Verification of user registration from servlet succeeded. Redirecting to success page");
					RequestDispatcher rd = request.getRequestDispatcher("submit-success.html");
					rd.forward(request, response);
				} else {
					log.info("User registration from servlet failed. Redirecting to failure page");
					if(user.isDataPresent()) {
						//sending flag to notify that user already registered with this emailId
						log.error("User registration from servlet failed : Email Id already registered!");
						RequestDispatcher rd = request.getRequestDispatcher("failed-submit-invalid-email.html");
						rd.forward(request, response);
					} else {
						log.error("User registration from servlet failed : Server Error!");
						RequestDispatcher rd = request.getRequestDispatcher("failed-submit.html");
						rd.forward(request, response);
					}
				}
			}
			else {
				//sending flag to notify that user entered invalid address
				log.error("User registration from servlet failed. Redirecting to failure page : Address is invalid!");
				RequestDispatcher rd = request.getRequestDispatcher("failed-submit-invalid-address.html");
				rd.forward(request, response);
			}
		} catch (IOException e) {
			log.error("Something went wrong in regsitration from servlet, IOException caught!", e);
		} catch (NullPointerException e) {
			log.error("Something went wrong in regsitration from servlet, NullPointerException caught!", e);
		} catch (Exception e) {
			log.error("Something went wrong in regsitration from servlet, Exception caught!", e);
		}
	}
	
	/**
	 * 
	 * @param searchedAddressString
	 * @throws NullPointerException
	 * @throws Exception
	 * 
	 * Parses the given address into it's individual address fields.
	 */
    public void parseAddress(String searchedAddressString) throws NullPointerException, Exception {
		
    	log.info("Parsing User Address");

    	HttpURLConnection conn = null;
    	
    	try {

			URL url = new URL(getParseAPIURL(searchedAddressString));
			
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
	 		
			log.debug("\nSending 'GET' request to Addressify URL");
			
			int responseCode = conn.getResponseCode();
			log.info("Response Code : " + responseCode);
			
			if(responseCode == HttpURLConnection.HTTP_OK) {
				BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				String inputLine = in.readLine();
	
				JSONParser parse = new JSONParser();
				JSONObject jobj = (JSONObject)parse.parse(inputLine);
				streetAddress = (String) jobj.get("StreetLine");
				suburb = (String) jobj.get("Suburb");
				state = (String) jobj.get("State");
				postcode = (String) jobj.get("Postcode");
				
				in.close();
			}
			else if(responseCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
				log.error("Address validation request is Unathorized and failed with response message : " + conn.getResponseMessage());
			}
			else if(responseCode == HttpURLConnection.HTTP_BAD_REQUEST) {
				log.error("Address validation request is Bad Request and failed with response message : " + conn.getResponseMessage());
			}
    	} catch (MalformedURLException e) {

			log.error("Something went wrong in login servlet, MalformedURLException caught!", e);

		}
		catch (IOException e) {
			log.error("Something went wrong in login servlet, IOException caught!", e);

		} finally {
			conn.disconnect();
		}
	}
    
    /**
     * 
     * @param addressTobeValidated
     * @return
     * @throws ParseException
     * 
     * Checks whether the given address is valid. Please note that validation is only performed on the street, suburb, state and postcode. 
     * Street, unit and level numbers are not checked for validity.
     */
    private boolean validateAddress(String addressTobeValidated) throws ParseException {
    	log.info("Validating User Address");

    	HttpURLConnection conn = null;
    	boolean validAddress = false;
    	
    	try {

			URL url = new URL(getValidateAPIURL(addressTobeValidated));
			
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
	 		
			log.debug("\nSending 'GET' request to Addressify Validate URL");
			
			int responseCode = conn.getResponseCode();
			log.info("Response Code : " + responseCode);
			
			if(responseCode == HttpURLConnection.HTTP_OK) {
				BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				validAddress = Boolean.valueOf(in.readLine());
				
				if(validAddress) {
					log.info("Address is valid. Continuing to register user!");
				}
				else {
					log.info("Address is invalid. Sending back error to registration page!");
				}
				
				in.close();
			}
			else if(responseCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
				log.error("Address validation request is Unathorized and failed with response message : " + conn.getResponseMessage());
			}
			else if(responseCode == HttpURLConnection.HTTP_BAD_REQUEST) {
				log.error("Address validation request is Bad Request and failed with response message : " + conn.getResponseMessage());
			}
    	} catch (MalformedURLException e) {

			log.error("Something went wrong in login servlet, MalformedURLException caught!", e);

		}
		catch (IOException e) {
			log.error("Something went wrong in login servlet, IOException caught!", e);

		} finally {
			conn.disconnect();
		}
    	return validAddress;
    }
    
    /**
     * 
     * @param term
     * @return URL for invoking the addresslite parse api like - https://api.addressify.com.au/address/parse?api_key={your API key}&term=1+george+st+sydenham+nsw+2044
     * 
     */
    private String getParseAPIURL(String term) {
    	
    	StringBuilder url = new StringBuilder();
		url.append(PARSE_API_URL);
		url.append(QUERY_PARAM_API);
		url.append(API_KEY);
		url.append(QUERY_PARAM_TERM);
 
		try {
			String param = URLEncoder.encode(term, StandardCharsets.UTF_8.toString());
			log.info("Encoded param : " + param);
			url.append(param);
		}
		catch(UnsupportedEncodingException e) {
			log.error("Something went wrong in building addresify parse api URL, UnsupportedEncodingException caught!", e);
		}

		log.info("URL for Addressify Parse API : " + url);
		return url.toString();
	}
    
    /**
     * 
     * @param term
     * @return URL for invoking the addresslite validate api like - https://api.addressify.com.au/address/validate?api_key={your API key}&term=1+george+st+sydenham+nsw+2044
     */
    private String getValidateAPIURL(String term) {
	
    	StringBuilder url = new StringBuilder();
		url.append(VALIDATE_API_URL);
		url.append(QUERY_PARAM_API);
		url.append(API_KEY);
		url.append(QUERY_PARAM_TERM);
 
		try {
			String param = URLEncoder.encode(term, StandardCharsets.UTF_8.toString());
			log.info("Encoded param : " + param);
			url.append(param);
		}
		catch(UnsupportedEncodingException e) {
			log.error("Something went wrong in building addresify validate api URL, UnsupportedEncodingException caught!", e);
		}

		log.info("URL for Addressify Validate API : " + url);
		return url.toString();
	}
	
}
