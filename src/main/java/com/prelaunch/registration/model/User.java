package com.prelaunch.registration.model;

import java.io.Serializable;

public class User implements Serializable {

	    private static final long serialVersionUID = 1L;
	    private static final String SPACE = " ";
	    private String firstName;
	    private String middleName;
	    private String lastName;
	    private String dob;
	    private String gender;
	    private String contact;
	    private String email;
	    private String streetAddress;
	    private String suburb;
	    private String state;
	    private String postcode;
	    private boolean dataPresent;
	    
	    public String getFirstName() {
	        return firstName;
	    }
	    public void setFirstName(String firstName) {
	    	this.firstName = firstName;
	    }
	    public String getMiddlename() {
	    	return middleName;
	    }
	    public void setMiddlename(String middleName) {
	    	if(middleName == null || middleName.isEmpty() || middleName.length() == 0) 
	    		this.middleName = SPACE;
	    	else 
	    		this.middleName = middleName;
	    }
	    public String getLastName() {
	        return lastName;
	    }
	    public void setLastName(String lastName) {
	        this.lastName = lastName;
	    }
	    public String getDob() {
			return dob;
		}
		public void setDob(String dob) {
			if(dob == null || dob.isEmpty() || dob.length() == 0) 
	    		this.dob = SPACE;
	    	else 
	    		this.dob = dob;
		}
		public String getGender() {
			return gender;
		}
		public void setGender(String gender) {
			if(gender == null || gender.isEmpty() || gender.length() == 0) 
	    		this.gender = SPACE;
	    	else 
	    		this.gender = gender;
		}
		public String getContact() {
	        return contact;
	    }
	    public void setContact(String contact) {
	        this.contact = contact;
	    }
	    public String getEmail() {
	        return email;
	    }
	    public void setEmail(String email) {
	        this.email = email;
	    }
		public String getStreetAddress() {
			return streetAddress;
		}
		public void setStreetAddress(String streetAddress) {
			this.streetAddress = streetAddress;
		}
		public String getSuburb() {
			return suburb;
		}
		public void setSuburb(String suburb) {
			this.suburb = suburb;
		}
		public String getState() {
			return state;
		}
		public void setState(String state) {
			this.state = state;
		}
		public String getPostcode() {
			return postcode;
		}
		public void setPostcode(String postcode) {
			this.postcode = postcode;
		}
		public boolean isDataPresent() {
			return dataPresent;
		}
		public void setDataPresent(boolean dataPresent) {
			this.dataPresent = dataPresent;
		}
}
