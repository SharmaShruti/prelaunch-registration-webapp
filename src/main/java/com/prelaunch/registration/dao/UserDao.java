package com.prelaunch.registration.dao;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import lombok.extern.slf4j.Slf4j;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.PutItemRequest;
import com.amazonaws.services.dynamodbv2.model.PutItemResult;
import com.prelaunch.registration.model.User;


@DynamoDBTable(tableName="UserDetails")
@Slf4j
public class UserDao {

	private static AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard()
        	.withRegion(Regions.AP_SOUTHEAST_2)
            .build();
	private static DynamoDB dynamoDB = new DynamoDB(client);

	
	public boolean saveUserDetails(User user) throws ClassNotFoundException {
		
		log.info("Entering User Dao layer");

		boolean result = false;		
		UUID userId = UUID.randomUUID();

        try {

			log.info("Verifying if user already registered with email id : " + user.getEmail());
        	if(!isItemPresentAlready(user))
        	{
				log.info("Saving values into database");
	        	Map<String,AttributeValue> attributeValues = new HashMap<>();
				log.info("UserId for record : " + userId.toString());
	        	attributeValues.put("UserId",new AttributeValue().withS(userId.toString()));
	            attributeValues.put("FirstName",new AttributeValue().withS(user.getFirstName()));
	            attributeValues.put("MiddleName",new AttributeValue().withS(user.getMiddlename()));
	            attributeValues.put("LastName",new AttributeValue().withS(user.getLastName()));
	            attributeValues.put("DOB",new AttributeValue().withS(user.getDob()));
	            attributeValues.put("Gender",new AttributeValue().withS(user.getGender()));
	            attributeValues.put("Contact",new AttributeValue().withS(user.getContact()));
	            attributeValues.put("EmailId",new AttributeValue().withS(user.getEmail()));
	            attributeValues.put("StreetAddress",new AttributeValue().withS(user.getStreetAddress()));
	            attributeValues.put("Suburb",new AttributeValue().withS(user.getSuburb()));
	            attributeValues.put("State",new AttributeValue().withS(user.getState()));
	            attributeValues.put("Postcode",new AttributeValue().withS(user.getPostcode()));
	            attributeValues.put("DateOfRegistration",new AttributeValue().withS(getCurrentDateForRegistration()));
	            
	            PutItemRequest putItemRequest = new PutItemRequest()
	                    .withTableName("UserDetails")
	                    .withItem(attributeValues);
	            PutItemResult putItemResult = client.putItem(putItemRequest);
	            
	            result = putItemResult != null ? true : false;
	            
	            if(result) {
					log.info("Data insertion was successfull");
	            } else
					log.info("Data insertion failed");
        	} 
        	else {
        		user.setDataPresent(true);
				log.info("User already registered with this email Id");
        	}
        } catch (Exception e) {
			log.error("Something went wrong in regsitration from Dao layer, Exception caught!", e);
        }
        
        return result;
	}
	
	private static boolean isItemPresentAlready(User user) {

		log.info("Entering verification for email " + user.getEmail());
		
		boolean itemExists = false;
		Table table = dynamoDB.getTable("UserDetails");
		GetItemSpec spec = new GetItemSpec().withPrimaryKey("EmailId", user.getEmail());
		
		try {
			log.info("Attempting to read the item...");
			Item outcome = table.getItem(spec);
			
			if(outcome == null) {
				log.info("Item doesn't present : " + outcome);
			}
			else {
				log.info("Item already present : " + outcome);
				itemExists= true;
			}
        }
        catch (Exception e) {
			log.error("Something went wrong in verifying user data for email id " + user.getEmail() + ", Exception caught!", e);
        }
		 
		return itemExists;
	}
	
	private static String getCurrentDateForRegistration() {
		LocalDateTime now = LocalDateTime.now();  
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");  
        String formattedDateTime = now.format(format);  
		log.info("Date and Time for User Regsitration : " + formattedDateTime);  
		 
		return formattedDateTime;
	}
	
}
