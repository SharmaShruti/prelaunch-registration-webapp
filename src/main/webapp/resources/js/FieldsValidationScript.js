var invalidAddress = true;
var getAddresses = function( request, response ) {
    $.post("https://event.openshop.com.au/getAddress", { userAddrInput: $("#addressid").val() })
    .done(function(data) {
        response(data)
     })
    .fail(function(data) {
    	document.getElementById('address_msg').innerHTML="Server error occurred. Please try again later!";
    })
};

var selectItem = function (event, ui) {
    $("#addressid").val(ui.item.value);
    invalidAddress = false;
};

$(document).ready(function(){
  $("#addressid").autocomplete({
      source: getAddresses,
      select: selectItem,
      minLength: 3,
      delay: 500
  });
});

function callRegistrationServlet(form) {
	if(validateFormDetails(form)) {
		document.register.action="registerUser";
		document.register.method="post";
		document.register.submit();
	}
	else {
		return false;
	}
}

function validateFormDetails(form) {
	var isValid = false;

	if (validateFirstName(form) && validateMiddleName(form)
			&& validateLastName(form) && validateDateOfBirth(form)
			&& validatePhone(form) && validateEmail(form)
			&& validateAddress(form) && validateConsent(form)) {
		isValid = true;
	}

	return isValid;
}

function validateFirstName(form) {
	if(form.firstName.value == "") {
		document.getElementById('fname_msg').innerHTML="Please provide your First Name!";
        form.firstName.focus() ;
        return false;
	}
	else {
		var regexForName = /^[ A-Za-zéæ'-.\s]*$/i;
		if(!regexForName.test(form.firstName.value)) {
			var name = document.getElementById('firstName');
			document.getElementById('fname_msg').innerHTML="You have entered an invalid Name!";
			form.firstName.focus() ;
		    return false;
		}
		document.getElementById('fname_msg').innerHTML="";
	}
	return true;
}

function validateMiddleName(form) {
	if(form.middleName.value != "") {
		var regexForName = /^[ A-Za-zéæ'-.\s]*$/i;
		if(!regexForName.test(form.middleName.value)) {
			document.getElementById('mname_msg').innerHTML="You have entered an invalid Middle Name!";
			form.middleName.focus() ;
		    return false;
		}
		document.getElementById('mname_msg').innerHTML="";
	}
	else {
		document.getElementById('mname_msg').innerHTML="";
	}
	return true;
}

function validateLastName(form) {
	if(form.lastName.value == "") {
		document.getElementById('lname_msg').innerHTML="Please provide your Last Name!";
        form.lastName.focus() ;
        return false;
	}
	else {
		var regexForName = /^[ A-Za-zéæ'-.\s]*$/i;
		if(!regexForName.test(form.lastName.value)) {
			document.getElementById('lname_msg').innerHTML="You have entered an invalid Last Name!";
			form.lastName.focus() ;
		    return false;
		}
		document.getElementById('lname_msg').innerHTML="";
	}
	return true;
}

function validateDateOfBirth(form) {
	if(form.dob.value != "") {
	  var regexForDOB = /^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/g;
	  if(!regexForDOB.test(form.dob.value)) {
		  document.getElementById('dob_msg').innerHTML="You have entered an invalid Date!";
	      form.dob.focus() ;
	      return false;
	  }
	  document.getElementById('dob_msg').innerHTML="";
	}
	else {
		 document.getElementById('dob_msg').innerHTML="";
	}
	return true;
}

function validatePhone(form) {
	if(form.contact.value == "") {
	   document.getElementById('contact_msg').innerHTML="Please provide your Phone Number!";
       form.contact.focus() ;
       return false;
	}
	else {
		var regexForPhone = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
		if(!regexForPhone.test(form.contact.value)) {
			document.getElementById('contact_msg').innerHTML="You have entered an invalid Phone Number!";
			form.contact.focus() ;
		    return false;
		}
		document.getElementById('contact_msg').innerHTML="";
	}
	return true;
}

function validateEmail(form) {
	if(form.email.value == "") {
		  document.getElementById('email_msg').innerHTML="Please provide your Email!";
	      form.email.focus() ;
	      return false;
	}
	else {
		var regexForEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
		if(!regexForEmail.test(form.email.value)) {
			document.getElementById('email_msg').innerHTML="You have entered an invalid email address!";
			form.email.focus() ;
		    return false;
		}
		document.getElementById('email_msg').innerHTML="";
	}
	return true;
}

function validateAddress(form) {
	if(form.address.value == "" || invalidAddress) {
	 document.getElementById('address_msg').innerHTML="Please provide a valid Address!";
     form.address.focus() ;
     return false;
	}
	else {
		document.getElementById('address_msg').innerHTML="";
	}
	return true;
}

function validateConsent(form) {
	if(!form.consent.checked) {
		alert("Please accept the Terms and Conditions!");
		form.consent.focus();
		return false;
    }
	return true;
}

window.onload = function () {
    document.getElementById("fname_msg").oninput = validateFirstName;
	document.getElementById("mname_msg").oninput = validateMiddleName;
	document.getElementById("lname_msg").oninput = validateLastName;
	document.getElementById("dob_msg").oninput = validateDateOfBirth;
	document.getElementById("contact_msg").oninput = validatePhone;
	document.getElementById("email_msg").oninput = validateEmail;
	document.getElementById("address_msg").oninput = validateAddress;
}